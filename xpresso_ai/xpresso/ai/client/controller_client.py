__all__ = ['ControllerClient']
__author__ = 'Sahil Malav'

import os
import subprocess
import time
import zipfile

import requests
import xpresso.ai.core.data.versioning.controller_factory \
    as version_controller_factory
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    ControllerClientResponseException, PermissionDeniedException
from xpresso.ai.core.commons.exceptions.xpr_exceptions import XprExceptions
from xpresso.ai.core.commons.network.http.http_request import HTTPMethod
from xpresso.ai.core.commons.network.http.send_request import SendHTTPRequest
from xpresso.ai.core.commons.utils import error_codes
from xpresso.ai.core.commons.utils.constants import TMP_DIR, INPUT_PROJECT_NAME
from xpresso.ai.core.commons.utils.generic_utils import get_version, \
    get_base_pkg_location, encrypt_string
from xpresso.ai.core.commons.utils.linux_utils import check_root
from xpresso.ai.core.commons.utils.user_utils import save_token, get_token, \
    get_workspace_file_path, get_token_file_path
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser
from xpresso.ai.core.logging.xpr_log import XprLogger


class ControllerClient:
    CONTROLLER_SECTION = 'controller'
    SERVER_URL = 'server_url'
    CLIENT_PATH = 'client_path'
    BUILD_CONFIG = "build_management"
    BUILD_TOOL = 'build_tool'
    BUILD_HOST = 'master_host'
    relogin_response = {
        "outcome": "failure",
        "error_code": "106",
        "results": {}
    }

    API_JSON_OUTCOME = "outcome"
    API_JSON_RESULTS = "results"
    API_JSON_ERROR_CODE = "error_code"
    API_JSON_SUCCESS = "success"
    API_JSON_FAILURE = "failure"

    def __init__(self, config_path=XprConfigParser.DEFAULT_CONFIG_PATH):
        self.logger = XprLogger(config_path=config_path)
        self.config = XprConfigParser(config_path)
        self.path = os.path.join(
            os.path.expanduser('~'),
            self.config[self.CONTROLLER_SECTION][self.CLIENT_PATH])
        self.workspace_file = f'{self.path}.workspace'
        self.token_file = '{}.current'.format(self.path)
        self.server_path = self.config[self.CONTROLLER_SECTION][self.SERVER_URL]

    def url_info(self):
        """
        Gives links for bitbucket and other platform
        :return:
        """
        headers = {"token": get_token(self.token_file)}
        try:
            url = f"{self.server_path}/info"
            json_response = self.send_http_request(url=url, header=headers,
                                                   http_method=HTTPMethod.GET)
            return json_response
        except ControllerClientResponseException:
            self.logger.warning("No Token found")

    def sso_login(self):
        """ It performs Single Sign-On authentication for the client.
        It follows following steps
        1. Check if token exists
        2. If exists: Send to the api_server for validation
            2.1 If token is validated then login is successful
            2.2 If token is not validated, assume token does not exist and go
            to point 3
        3. If no token exists:
            3.1 Print the SSO authentication url for user to login
            3.2 Send request to api_server every few seconds to check if user
            signed in successful. Wait for 60 seconds. Throw error if not
            logged in
            3.3 When user logged in, fetch the token and save

        """
        self.logger.info('CLIENT : Entering SSO Login Method')

        # Check if token exists:
        try:
            token = get_token(self.token_file)
        except ControllerClientResponseException:
            self.logger.warning("No Token found")
            token = None

        # Since no token exist, ask for new login
        if token:
            url = f"{self.server_path}/sso/token_login"
            self.logger.debug('CLIENT : Making post request to api_server')
            data = {"token": token}
            try:
                response = self.send_http_request(url=url,
                                                  header=data,
                                                  http_method=HTTPMethod.POST,
                                                  data=data)
                return response
            except ControllerClientResponseException as e:
                self.logger.warning("Assuming logging request failed")
                self.logger.warning(e.message)

        url = f"{self.server_path}/sso/get_authentication_url"
        self.logger.debug('CLIENT : Making post request to api_server')
        response = self.send_http_request(url=url,
                                          http_method=HTTPMethod.GET)
        return response

    def sso_validate(self, validation_token):
        """
        Check whether SSO authentication is completed and successful
        Args:
            validation_token: sso validation token which is used to check if
                              a user has logged in or not.
        Returns:
        """
        # We keep requesting the sso api_server to test for
        interval_second = 2
        wait_second = 60
        start_time = time.time()
        while time.time() - start_time < wait_second:
            self.logger.debug('CLIENT : Making post request to api_server')
            url = f"{self.server_path}/sso/validate"
            data = {"validation_token": validation_token}
            try:

                response = self.send_http_request(url=url,
                                                  http_method=HTTPMethod.POST,
                                                  data=data)
                self.logger.info("Token validated")
                save_token(self.token_file, response["token"])
                return {"message": "SSO Login Successfull"}
            except ControllerClientResponseException:
                time.sleep(interval_second)
        self.logger.info('CLIENT : Existing SSO Login Method')
        raise ControllerClientResponseException(
            "Session over without login", error_codes.server_error)

    def login(self, username, password):
        """Sends request to Controller api_server and
        get the status on login request"""
        self.logger.info('CLIENT : entering login method')
        self.workspace_file = get_workspace_file_path(self.path)
        self.token_file = get_token_file_path(self.path)

        if not os.path.isdir(self.path):
            os.makedirs(self.path, 0o755)
        if os.path.isfile(self.token_file):
            os.remove(self.token_file)

        if not username:
            self.logger.error('CLIENT : Empty username passed. Exiting.')
            raise ControllerClientResponseException(
                "Username can't be empty", error_codes.empty_uid)
        if not password:
            self.logger.error('CLIENT : Empty password passed. Exiting.')
            raise ControllerClientResponseException(
                "Password can't be empty", error_codes.empty_uid)

        url = f"{self.server_path}/auth"
        credentials = {"uid": username, "pwd": encrypt_string(password)}
        self.logger.debug('CLIENT : Making post request to api_server')
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          data=credentials)
        save_token(self.token_file, token=response['access_token'])
        if 'relogin' in response and response['relogin']:
            self.logger.debug(
                'CLIENT : already logged in.')
            return {"message": f"You are already logged in"}
        elif 'relogin' in response and not response['relogin']:
            self.logger.info(
                'CLIENT : Login successful. Writing token to file.')
            return {"message": f"You are now logged in to Xpresso IDDME. \n"
                               f"Welcome, {username}! "}
        return response

    def logout(self):
        self.logger.info('CLIENT : entering logout method')
        url = f'{self.server_path}/auth'
        self.token_file = get_token_file_path(self.path)
        token = get_token(self.token_file)
        headers = {'token': token}
        self.logger.debug('CLIENT : Making delete request to api_server')
        self.send_http_request(url=url,
                               http_method=HTTPMethod.DELETE,
                               header=headers)
        os.remove(self.token_file)
        os.environ['CURRENT_USER'] = ''
        self.logger.info('CLIENT : Logout successful. Exiting.')
        return {"message": "Successfully logged out"}

    def get_clusters(self, argument):
        self.logger.info(f'CLIENT : entering get_clusters method '
                         f'with arguments {argument}')
        url = f'{self.server_path}/clusters'
        headers = {"token": get_token(self.token_file)}
        self.logger.debug('CLIENT : Making get request to api_server')
        response = self.send_http_request(url=url, http_method=HTTPMethod.GET,
                                          header=headers, data=argument)
        self.logger.info('CLIENT : Get request successful. Exiting.')
        return response

    def deactivate_cluster(self, argument):

        self.logger.info('CLIENT : Entering deactivate_cluster method')
        if not argument:
            self.logger.error('CLIENT : No input arguments provided. Exiting.')
            raise ControllerClientResponseException(
                f"Please provide some input arguments ===",
                error_codes.incomplete_cluster_info)
        url = f'{self.server_path}/clusters'
        headers = {"token": get_token(self.token_file)}
        self.send_http_request(url=url, http_method=HTTPMethod.DELETE,
                               header=headers, data=argument)
        self.logger.info('CLIENT : Deactivation successful. Exiting.')
        return {"message": "Cluster deactivated."}

    def register_cluster(self, argument):
        self.logger.info('CLIENT : Entering register_cluster '
                         'with arguments {}'.format(argument))
        if not argument:
            self.logger.error('CLIENT : No input arguments provided. Exiting.')
            raise ControllerClientResponseException(
                f"Please provide some input arguments ===",
                error_codes.incomplete_cluster_info)
        url = f'{self.server_path}/clusters'
        headers = {"token": get_token(self.token_file)}
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          header=headers, data=argument)
        self.logger.info(
            'CLIENT : Cluster registration successful.Exiting.')
        return {
            "message": f"Cluster successfully registered with ID {response} ###"}

    def register_user(self, user_json):
        url = f"{self.server_path}/users"
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=user_json)
        return response

    def get_users(self, filter_json):
        url = f"{self.server_path}/users"
        response = self.send_http_request(url=url, http_method=HTTPMethod.GET,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=filter_json)
        return response

    def modify_user(self, changes_json):
        url = f"{self.server_path}/users"
        response = self.send_http_request(url=url, http_method=HTTPMethod.PUT,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=changes_json)
        return response

    def update_password(self, password_json):
        url = f"{self.server_path}/user/pwd"
        response = self.send_http_request(url=url, http_method=HTTPMethod.PUT,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=password_json)
        return response

    def deactivate_user(self, uid_json):
        url = f"{self.server_path}/users"
        response = self.send_http_request(url=url,
                                          http_method=HTTPMethod.DELETE,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=uid_json)
        return response

    def register_node(self, node_json):
        url = f"{self.server_path}/nodes"
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=node_json)
        return response

    def get_nodes(self, filter_json):
        url = f"{self.server_path}/nodes"
        response = self.send_http_request(url=url, http_method=HTTPMethod.GET,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=filter_json)
        return response

    def provision_node(self, changes_json):
        url = f"{self.server_path}/nodes"
        response = self.send_http_request(url=url, http_method=HTTPMethod.PUT,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=changes_json)
        return response

    def deactivate_node(self, node_json):
        url = f"{self.server_path}/nodes"
        response = self.send_http_request(url=url,
                                          http_method=HTTPMethod.DELETE,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=node_json)
        return response

    def assign_node(self, assign_json):
        url = f"{self.server_path}/assign_node"
        response = self.send_http_request(url=url, http_method=HTTPMethod.PUT,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=assign_json)
        return response

    def create_project(self, project_json):
        url = f"{self.server_path}/projects/manage"
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=project_json)
        response["message"] = "Project created successfully!"
        return response

    def get_project(self, filter_json):
        url = f"{self.server_path}/projects/manage"
        response = self.send_http_request(url=url, http_method=HTTPMethod.GET,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=filter_json)
        return response

    def modify_project(self, changes_json):

        message = "Project successfully modified."

        url = f"{self.server_path}/projects/manage"
        response = self.send_http_request(url=url, http_method=HTTPMethod.PUT,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=changes_json)
        return response

    def deactivate_project(self, project_json):
        url = f"{self.server_path}/projects/manage"
        response = self.send_http_request(url=url,
                                          http_method=HTTPMethod.DELETE,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=project_json)
        return response

    def submit_notebook(self, notebook_json):
        url = f"{self.server_path}/submit/notebook"
        response = self.send_http_request(url=url,
                                          http_method=HTTPMethod.POST,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=notebook_json)
        return response

    def build_project(self, argument):
        self.logger.info(f'CLIENT : Entering build_project '
                         f'with arguments {argument}')
        if not argument:
            self.logger.error('CLIENT : No input arguments provided. Exiting.')
            raise ControllerClientResponseException(
                f"Please provide some input arguments ===",
                error_codes.incomplete_cluster_info)
        url = f'{self.server_path}/projects/build'
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=argument)
        self.logger.info('CLIENT : Project build successful.Exiting.')
        return response

    def get_build_version(self, argument):
        self.logger.info(f'CLIENT : entering get_build_version method '
                         f'with arguments {argument}')
        url = f'{self.server_path}/projects/build'
        self.logger.debug('CLIENT : Making get request to api_server')
        response = self.send_http_request(url=url, http_method=HTTPMethod.GET,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=argument)
        return response

    def deploy_project(self, argument):
        self.logger.info(f'CLIENT : Entering deploy_project '
                         f'with arguments {argument}')
        if not argument:
            self.logger.error('CLIENT : No input arguments provided. Exiting.')

            raise ControllerClientResponseException(
                f"Please provide some input arguments ===",
                error_codes.incomplete_cluster_info)
        url = f'{self.server_path}/projects/deploy'
        response = self.send_http_request(url=url, http_method=HTTPMethod.POST,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=argument)
        self.logger.info(
            'CLIENT : Project deployed successfully.Exiting.')
        return {"message": "Project deployed successfully on the below IPs!",
                "Output": response}

    def undeploy_project(self, argument):
        self.logger.info(f'CLIENT : Entering undeploy_project '
                         f'with arguments {argument}')
        if not argument:
            self.logger.error('CLIENT : No input arguments provided. Exiting.')
            raise ControllerClientResponseException(
                f"Please provide some input arguments ===",
                error_codes.incomplete_cluster_info)

        url = f'{self.server_path}/projects/deploy'
        response = self.send_http_request(url=url,
                                          http_method=HTTPMethod.DELETE,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=argument)
        self.logger.info(
            'CLIENT : Project undeployed successfully.Exiting.')
        return {"message": "Project undeployed successfully!"}

    def get_pipeline_versions(self, pipeline_info):
        """
        fetches the pipeline versions of a deployed project

        Args:
            pipeline_info: information on pipeline
        Returns:
            returns list of pipeline versions with info
        """
        url = f'{self.server_path}/projects/deploy'
        response = self.send_http_request(url=url, http_method=HTTPMethod.GET,
                                          header={"token": get_token(
                                              self.token_file)},
                                          data=pipeline_info)
        self.logger.info(
            'CLIENT : Fetched pipeline versions: ', response)
        return response

    def download_file(self, url):
        """
        Downloads zipfile containing latest xpresso version and writes it to
        local system
        Args:
            url: web url of the file to download
        Returns:
            str: path of created zipfile
        """
        try:
            response = requests.get(url)
            os.chdir("/tmp")
            filename = "xpresso_zip"
            subprocess.run(args=["touch", f"{filename}"])
            open(filename, 'wb').write(response.content)
            return os.path.abspath(filename)

        except OSError as err:
            self.logger.error(err)
            raise err

        except RuntimeError as err:
            self.logger.error(err)
            raise err

        except SystemError as err:
            self.logger.error(err)
            raise err

    def update_xpresso(self):
        """
        Update xpresso project to the latest commit
        """
        version = self.fetch_version()
        try:
            user = check_root()
            if user and (version["client_version"] < version["server_version"]):
                pip_version = "pip3"
                url = f'{self.server_path}/update_xpresso'
                xpresso_zipfile = self.download_file(url)
                directory_to_extract_to = "/opt"
                subprocess.run(args=["mv", get_base_pkg_location(), TMP_DIR])
                with zipfile.ZipFile(xpresso_zipfile, 'r') as zip_ref:
                    zip_ref.extractall(directory_to_extract_to)
                subprocess.run(["mv", xpresso_zipfile, TMP_DIR])
                os.chdir("opt/xpresso.ai")
                var = subprocess.run(args=["pip3", "--version"],
                                     stderr=subprocess.PIPE, text=True).stderr
                if var:
                    pip_version = "pip"
                subprocess.run(
                    f"{pip_version} install --upgrade -r requirements.txt",
                    shell=True)
                # subprocess.run(f"{pip_version} install --upgrade", shell=True)
                return {"message": "Xpresso is updated to latest commit!"}
            else:
                return {
                    "message": "Xpresso is already updated to latest commit!"}

        except PermissionDeniedException as err:
            self.logger.error(err)
            return {"message": err.message}

        except OSError as err:
            self.logger.error(err)
            raise err

        except RuntimeError as err:
            self.logger.error(err)
            raise err

        except SystemError as err:
            self.logger.error(err)
            raise err

        except zipfile.BadZipFile as err:
            self.logger.error(err)
            raise err

        except zipfile.LargeZipFile as err:
            self.logger.error(err)
            raise err

    def fetch_version(self):
        """
        Fetches api_server version and client version, convert to a dict and
        returns.
        """
        url = f'{self.server_path}/version'
        json_response = self.send_http_request(url, HTTPMethod.GET)
        server_version = "None"
        if "version" in json_response:
            server_version = json_response["version"]
        client_version = get_version()
        return {
            "client_version": client_version,
            "server_version": server_version
        }

    @staticmethod
    def send_http_request(url: str, http_method: HTTPMethod,
                          data=None, header: dict = None):
        return SendHTTPRequest().send(url=url, http_method=http_method,
                                      data=data, header=header)

    def create_repo(self, repo_json):
        """
        creates a repo on pachyderm cluster

        :param repo_json:
            information of repo i.e. name and description
        :return:
            returns operation status
        """
        url = f"{self.server_path}/repo/create"
        user_token = get_token(self.token_file)
        self.send_http_request(url=url, http_method=HTTPMethod.POST,
                               header={"token": user_token})
        credentials = {"token": user_token, "env": self.get_workspace_env()}
        try:
            controller_factory = version_controller_factory.VersionControllerFactory(
                **credentials)
            version_controller = controller_factory.get_version_controller()
            version_controller.create_repo(**repo_json)
            response = {"message": "repo created successfully"}
            return response
        except XprExceptions as err:
            return err.message

    def list_repos(self):
        """
        lists all the pachyderm repos a user has access to
        :return:
            returns list of repos
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        try:
            controller_factory = version_controller_factory.VersionControllerFactory(
                **credentials)
            version_controller = controller_factory.get_version_controller()
            repos = version_controller.list_repo()
            return repos
        except XprExceptions as err:
            return err.message
        except Exception as err:
            raise err

    def list_branches(self, repo_json: dict):
        """
            lists all the branches for a pachyderm repo a user has access to
            :return:
                returns list of branches
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        try:
            controller_factory = version_controller_factory.VersionControllerFactory(
                **credentials)
            version_controller = controller_factory.get_version_controller()
            branches = version_controller.list_branch(repo_json["repo_name"])
            return branches
        except XprExceptions as err:
            return err.message
        except Exception as err:
            raise err

    def list_commits(self, repo_json: dict):
        """
            lists all the branches for a pachyderm repo a user has access to
            :return:
                returns list of branches
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        try:
            controller_factory = version_controller_factory.VersionControllerFactory(
                **credentials)
            version_controller = controller_factory.get_version_controller()
            commits = version_controller.list_commit(repo_json["repo_name"], repo_json["branch_name"])
            return commits
        except XprExceptions as err:
            return err.message
        except Exception as err:
            raise err

    def create_branch(self, branch_json):
        """
        creates a branch in a repo

        :param branch_json:
            information of branch i.e. repo and branch names
        :return:
            operation status
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        try:
            controller_factory = version_controller_factory.VersionControllerFactory(
                **credentials)
            version_controller = controller_factory.get_version_controller()
            version_controller.create_branch(**branch_json)
            response = {"message": "Branch created successfully"}
            return response
        except XprExceptions as err:
            return err.message

    def push_dataset(self, dataset_json):
        """
        pushes a dataset into pachyderm cluster

        :param dataset_json:
            information of dataset
        :return:
            operation status
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        controller_factory = version_controller_factory.VersionControllerFactory(
            **credentials)
        version_controller = controller_factory.get_version_controller()
        try:
            dataset_json["data_type"] = "files"
            commit_id, remote_path = version_controller.push_dataset(
                **dataset_json)
            return {
                "message": f"Dataset push successful. "
                           f"commit id: {commit_id}. "
                           f"remote path: {remote_path}"
            }
        except XprExceptions as err:
            return err.message

    def pull_dataset(self, dataset_json):
        """
        pulls a dataset from pachyderm cluster

        :param dataset_json:
            info of the dataset on pachyderm cluster
        :return:
            path of the dataset on user system
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        controller_factory = version_controller_factory.VersionControllerFactory(
            **credentials)
        version_controller = controller_factory.get_version_controller()
        try:
            dataset_json["output_type"] = "files"
            dataset_path = version_controller.pull_dataset(**dataset_json)
            return {
                "message": f"Pull Successful, find the files at {dataset_path}"}
        except XprExceptions as err:
            return err.message

    def list_dataset(self, filter_json):
        """
        lists dataset saved on pachyderm cluster as per filter specs

        :param filter_json:
            info to filter required dataset
        :return:
            list of all the files and their props as per filter specs
        """
        credentials = {"token": get_token(self.token_file),
                       "env": self.get_workspace_env()
                       }
        controller_factory = version_controller_factory.VersionControllerFactory(
            **credentials)
        version_controller = controller_factory.get_version_controller()
        try:
            dataset_list = version_controller.list_dataset(**filter_json)
            return dataset_list
        except XprExceptions as err:
            return err.message

    def get_workspace_env(self):
        self.workspace_file = get_workspace_file_path(self.path)
        with open(self.workspace_file, "r") as env_file:
            env = env_file.read()
        return env

    def start_experiment(self, experiment_info):
        """
        create a new experiment as per provided configurations
        Args:
            experiment_info (dict): info required to create new experiment
        Returns:
            returns success info
        """
        manage_experiments_url = f"{self.server_path}/experiments/start"
        user_token = get_token(self.token_file)
        workspace_env = self.get_workspace_env()
        response = self.send_http_request(
            url=manage_experiments_url, http_method=HTTPMethod.POST,
            data=experiment_info, header={"token": user_token,
                                          "env": workspace_env}
        )
        self.logger.info("start experiment successful")
        return response

    def get_experiment_details(self, experiment_info, verbose=False):
        """
        create a new experiment as per provided configurations
        Args:
            verbose: check if verbose output is required
            experiment_info (dict): info required to create new experiment
        Returns:
            returns success info
        """
        manage_experiments_url = f"{self.server_path}/experiments/manage"
        user_token = get_token(self.token_file)
        populated_experiment_info = {
            "experiments": [experiment_info],
            "verbose": verbose
        }
        response = self.send_http_request(
            url=manage_experiments_url, http_method=HTTPMethod.GET,
            data=populated_experiment_info, header={"token": user_token}
        )

        self.logger.info(f"get experiment response : {response}")
        return response

    def compare_experiments(self, experiments_info):
        """
        fetches the details of each experiment from database and returns it

        Args:
            experiments_info: info of all the experiments
        Returns:
            returns a list with info on experiments
        """
        manage_experiments_url = f"{self.server_path}/experiments/manage"
        user_token = get_token(self.token_file)
        response = self.send_http_request(
            url=manage_experiments_url, http_method=HTTPMethod.GET,
            data=experiments_info, header={"token": user_token}
        )

        self.logger.info(f"compare_experiments output: {response}")
        return response

    def pause_experiment(self, run_info: dict):
        """
        pauses an ongoing experiment

        Args:
            run_info: info on an experiment run
        Returns:
            returns if any info on this operation
        """
        control_experiment_url = f"{self.server_path}/experiment/control"
        user_token = get_token(self.token_file)
        response = self.send_http_request(
            url=control_experiment_url, http_method=HTTPMethod.POST,
            data=run_info, header={"token": user_token}
        )
        self.logger.info(f"pause_experiment output: {response}")
        return response

    def restart_experiment(self, run_info: dict):
        """
        restarts an experiment that has been paused before

        Args:
            run_info: info of experiment run that needs to be restarted
        Returns:
            returns info of the new start
        """
        control_experiment_url = f"{self.server_path}/experiment/control"
        user_token = get_token(self.token_file)
        response = self.send_http_request(
            url=control_experiment_url, http_method=HTTPMethod.PUT,
            data=run_info, header={"token": user_token}
        )
        self.logger.info(f"restart_experiment output: {response}")
        return response

    def terminate_experiment(self, run_info: dict):
        """
        terminates an experiment

        Args:
            run_info: info on experiment run
        Returns:
        """
        control_experiment_url = f"{self.server_path}/experiment/control"
        user_token = get_token(self.token_file)
        response = self.send_http_request(
            url=control_experiment_url, http_method=HTTPMethod.DELETE,
            data=run_info, header={"token": user_token}
        )
        self.logger.info(f"terminate_experiment output: {response}")
        return response
