import abc
# from xpresso.ai.core.data.versioning.versioning_authenticator import VersioningAuthenticator
# auth = VersioningAuthenticator()
from xpresso.ai.core.commons.utils.data_utils import generate_single_table_df
from xpresso.ai.core.data.visualization.utils import detect_notebook


class VersionControllerInterface(metaclass=abc.ABCMeta):
    """"""
    def __init__(self, **kwargs):
        # __init__ of inherited controller class should make a call to
        # authenticate_session method of VersioningAuthenticator
        pass

    # @auth.authenticate_request
    @abc.abstractmethod
    def create_repo(self, *args, **kwargs):
        """"""

    # @auth.filter_repo
    @abc.abstractmethod
    def list_repo(self, show_table=False):
        """"""
        print("this getting printed")

    # @auth.authenticate_request
    @abc.abstractmethod
    def create_branch(self, *args, **kwargs):
        """"""

    @abc.abstractmethod
    def list_branch(self, *args):
        """"""

    # @auth.authenticate_request
    @abc.abstractmethod
    def push_dataset(self, *args, **kwargs):
        """"""

    # @auth.authenticate_request
    @abc.abstractmethod
    def pull_dataset(self, *args, **kwargs):
        """"""

    # @auth.authenticate_request
    @abc.abstractmethod
    def list_dataset(self, *args, **kwargs):
        """"""

    @staticmethod
    def update_list_response(data, show_table=False):
        """ If notebook is found, it returns the data in df, otherwise it will
        return data"""
        if detect_notebook() and show_table:
            from IPython.display import HTML, display_html
            return display_html(
                HTML(generate_single_table_df(data=data, max_depth=3).to_html(index=False)))
        return data
