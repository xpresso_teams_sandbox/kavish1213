from xpresso.ai.core.data.distributed.automl.distributed_structured_dataset import DistributedStructuredDataset
from xpresso.ai.core.data.versioning.pachyderm_controller import PachydermVersionController
from xpresso.ai.core.data.versioning.controller_interface \
    import VersionControllerInterface
from xpresso.ai.core.data.versioning.versioning_authenticator \
    import VersioningAuthenticator
from xpresso.ai.core.logging.xpr_log import XprLogger

auth = VersioningAuthenticator()


class HDFSVersionController(VersionControllerInterface):
    """
    Manages repos on pachyderm cluster
    """
    # user input variables
    INPUT_BRANCH_NAME = "branch_name"
    INPUT_COMMIT_ID = "commit_id"
    INPUT_DATASET_NAME = "dataset_name"
    INPUT_DESCRIPTION = "description"
    INPUT_REPO_NAME = "repo_name"
    INPUT_PATH = "path"
    INPUT_DATASET = "dataset"
    LIST = "list"
    PULL = "pull"
    DATA_TYPE = "data_type"
    DATA_TYPE_IS_FILE = "files"
    # Internal Code specific variables
    ACCEPTED_FIELD_NAME_REGEX = r"[\w, -]+$"
    DEFAULT_LIST_DATASET_PATH = "/"
    PICKLE_FILE_EXTENSION = ".pkl"
    PUSH_FILES_MANDATORY_FIELDS = ["repo_name", "branch_name", "data_type",
                                   "dataset_name", "path", "description"]
    PUSH_DATASET_MANDATORY_FIELDS = ["repo_name", "branch_name", "dataset",
                                     "description"]
    COMMIT_INFO_ID = "commit_id"
    COMMIT_INFO_BRANCH = "branch_name"
    # output variables
    REPO_OUTPUT_NAME = "repo_name"
    FILE_TYPE = "type"
    OUTPUT_COMMIT_FIELD = "commit"
    OUTPUT_COMMIT_ID = "id"
    OUTPUT_BRANCH_HEAD = "head"
    PULL_OUTPUT_TYPE = "output_type"
    OUTPUT_FILE_TYPE = "files"
    PROJECTS = "projects"
    SUPER_USER = "su"

    def __init__(self, pachyderm_controller_info, **kwargs):
        """
        constructor class for PachydermVersionController
        """
        super().__init__(**kwargs)
        self.logger = XprLogger()
        # Pachyderm version controller for metadata
        self.metadata_version_controller = PachydermVersionController(pachyderm_controller_info, **kwargs)
        auth.kwargs = kwargs
        auth.authenticate_session()

    @auth.authenticate_request
    def create_repo(self, **kwargs):
        """
        creates a new repo on pachyderm cluster

        Args:
            kwargs(keyword arguments):
                repo_name(str): name of the repo to be created
                description(str): brief description of this repo
        """
        return self.metadata_version_controller.create_repo(**kwargs)

    @auth.filter_repo
    def list_repo(self, show_table=False):
        """
        returns the list of all available repos

        :return:
            returns list of repos
        """
        return self.metadata_version_controller.list_repo(show_table=show_table)

    @auth.authenticate_request
    def create_branch(self, **kwargs):
        """
        creates a new branch in specified repo on pachyderm cluster

        Args:
            kwargs:
                info of the branch to be created
                keys - repo_name, branch_name
                repo_name: name of the repo
                branch_name: name of the branch
        :return:
        """
        return self.metadata_version_controller.create_branch(**kwargs)

    @auth.authenticate_request
    def list_branch(self, repo_name, show_table=False):
        """
        returns a list of branches in specified repo

        :param repo_name:
            name of the repo to list the branches
        """
        return self.metadata_version_controller.list_branch(repo_name, show_table=show_table)

    @auth.authenticate_request
    def push_dataset(self, **kwargs):
        """
        pushes file/files into a pachyderm cluster

        :param kwargs:
        :keyword Arguments:
            *repo_name:
                name of the repo
            *branch_name:
                name of the branch
            *dataset:
                AbstractDataset object with info on dataset
            *dataset_name:
                name of dataset that will be pushed to cluster
            *path:
                local path of file or list of files i.e a directory
            *description:
                A brief description on this push
        """
        new_commit_id = self.metadata_version_controller.push_dataset(**kwargs)
        # save data into hdfs
        kwargs[self.INPUT_DATASET].save_data(new_commit_id[0])
        return new_commit_id

    @auth.authenticate_request
    def pull_dataset(self, **kwargs):
        """
        pulls a dataset from pachyderm cluster and load it locally

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns the path of the directory where dataset is saved
        """
        list_output = self.list_dataset(**kwargs)
        output = self.metadata_version_controller.dataset_manager.pull(kwargs[self.INPUT_REPO_NAME], list_output)
        if self.PULL_OUTPUT_TYPE in kwargs and \
                kwargs[self.PULL_OUTPUT_TYPE] == self.OUTPUT_FILE_TYPE:
            return output
        new_dataset = DistributedStructuredDataset()
        new_dataset.load(output)
        if self.INPUT_COMMIT_ID in kwargs:
            commit_id = kwargs[self.INPUT_COMMIT_ID]
        else:
            commits =  self.list_commit(kwargs[self.INPUT_REPO_NAME], kwargs[self.INPUT_BRANCH_NAME])[-1]
            commit_id = commits.get(self.COMMIT_INFO_ID)
        new_dataset.load_data(commit_id)
        return new_dataset

    @auth.authenticate_request
    def list_dataset(self, show_table=False, **kwargs):
        """
        list of dataset as per provided information

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns a dict with file and commit info
        """
        return self.metadata_version_controller.list_dataset(show_table=show_table, **kwargs)

    def verify_dataset_input(self, **kwargs):
        """
        verifies input parameters for dataset operations i.e. push, pull & list

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
        """
        return self.metadata_version_controller.verify_dataset_input(**kwargs)

    def complete_dataset_info(self, **kwargs):
        """
        verifies if the provided info for a dataset is valid or not

        :param kwargs:
        :keyword Arguments:
            * repo_name:
                name of the repo
            * branch_name:
                name of the branch
            * path (Optional):
                path of the dir in which dataset might be present
                       path of the dataset
            * commit_id (Optional):
                id of the commit
        :return:
            returns updated info else throws an exception
            in case of error
        """
        return self.metadata_version_controller.complete_dataset_info(**kwargs)

    @auth.authenticate_request
    def list_commit(self, repo_name, branch_name, show_table=False):
        """
        lists all the commits in a repo under a branch

        :param repo_name:
            name of the repo
        :param branch_name:
            name of the branch
        :return:
            list of commits
        """
        return self.metadata_version_controller.list_commit(repo_name, branch_name, show_table=show_table)

    def validate_push_dataset_input(self, **kwargs):
        """

        :param kwargs:
        :return:
        """
        return self.metadata_version_controller.validate_push_dataset_input(**kwargs)
